/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jzavalagam.abstractclassesandinterfaces;

/**
 *
 * @author PROFESOR
 */
public class TestCircleRectangle {

    public static void main(String[] args) {
        System.out.println("TestCircleRectangle!");
        
        Circle circle = new Circle(5);
        Rectangle rectangle = new Rectangle(5,5);
        System.out.println("El círculo: "+circle.toString());
        System.out.println("El círculo: "+circle.toString());
        System.out.println("El área del círculo: "+circle.getArea());
        System.out.println("El perímetro del círculo: "+circle.getPerimetro());
        System.out.println("El área del rectángulo: "+rectangle.getArea());
        System.out.println("El perímetro del rectángulo: "+rectangle.getPerimetro());
        
    }
}
