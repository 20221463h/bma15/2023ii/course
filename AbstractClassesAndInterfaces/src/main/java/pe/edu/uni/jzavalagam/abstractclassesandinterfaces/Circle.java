/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jzavalagam.abstractclassesandinterfaces;

/**
 *
 * @author PROFESOR
 */
public class Circle extends GeometricObject{
 
    private double raiduis;

    public Circle() {
        this.raiduis = 1.0;
    }

    public Circle(double raiduis) {
        this.raiduis = raiduis;
    }

    public Circle(double raiduis, String color, boolean filled) {
        super(color, filled);
        this.raiduis = raiduis;
    }
    
   
    @Override
    public double getArea() {
        double area =this.raiduis*this.raiduis*Math.PI;
        return area;
    }

    @Override
    public double getPerimetro() {
        double perimetro = 2*Math.PI*this.raiduis;
        return perimetro;
    }
    
    
}
