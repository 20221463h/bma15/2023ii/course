/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jzavalagam.interfaces;

/**
 *
 * @author PROFESOR
 */
public class Tiger extends Animal implements Edible{

    @Override
    public String sound() {
        return "grrr!!!";
    }

    @Override
    public String howToEat() {
        return "fry it!!!";
    }
    
    
}
