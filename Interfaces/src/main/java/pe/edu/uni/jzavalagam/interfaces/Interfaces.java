/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jzavalagam.interfaces;

/**
 *
 * @author PROFESOR
 */
public class Interfaces {

    public static void main(String[] args) {
        System.out.println("Interfaces!");
        Object[] objects = {new Orange(),new Apple(),new Chiken(), new Tiger()};
        for(Object object : objects){
            if(object instanceof Animal){
                System.out.println(((Animal) object).sound());
            }
            if(object instanceof Fruit){
                System.out.println(((Fruit) object).howToEat());
            }
        }
    }
}
